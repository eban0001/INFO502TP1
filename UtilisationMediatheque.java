public class UtilisationMediatheque {

    public static void main(String[] args) {

        Mediatheque mediatheque = new Mediatheque();
        mediatheque.setNomProprietaire("Alice Dupont");

        Film film1 = new Film("Inception", new StringBuffer("MOV001"), 9, "Christopher Nolan", 2010);
        Film film2 = new Film("Le Parrain", new StringBuffer("MOV002"), 10, "Francis Ford Coppola", 1972);
        mediatheque.add(film1);
        mediatheque.add(film2);

        Livre livre1 = new Livre("1984", new StringBuffer("LIV001"), 9, "George Orwell", 328);
        Livre livre2 = new Livre("Les Misérables", new StringBuffer("LIV002"), 10, "Victor Hugo", 1232);
        mediatheque.add(livre1);
        mediatheque.add(livre2);

        System.out.println("Affichage de la médiathèque d'Alice Dupont :");
        System.out.println(mediatheque);

        Mediatheque autreMediatheque = new Mediatheque(mediatheque);
        autreMediatheque.setNomProprietaire("Bob Martin");

        Film film3 = new Film("Interstellar", new StringBuffer("MOV003"), 8, "Christopher Nolan", 2014);
        autreMediatheque.add(film3);

        System.out.println("\nAffichage de la médiathèque de Bob Martin :");
        System.out.println(autreMediatheque);
    }
}
