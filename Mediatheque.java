import java.util.Vector;

public class Mediatheque {

    private String nomProprietaire;
    private Vector<Media> collectionMedias;

    public Mediatheque() {
        this.nomProprietaire = "";
        this.collectionMedias = new Vector<>();
    }

    public Mediatheque(Mediatheque autreMediatheque) {
        this.nomProprietaire = autreMediatheque.nomProprietaire;
        this.collectionMedias = new Vector<>(autreMediatheque.collectionMedias);
    }

    public void add(Media media) {
        this.collectionMedias.add(media);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Médiathèque de ").append(nomProprietaire).append("\n");
        sb.append("Collection de médias : \n");
        
        for (Media media : collectionMedias) {
            sb.append(media.toString()).append("\n");
        }
        
        return sb.toString();
    }

    public String getNomProprietaire() {
        return nomProprietaire;
    }

    public void setNomProprietaire(String nomProprietaire) {
        this.nomProprietaire = nomProprietaire;
    }

    public Vector<Media> getCollectionMedias() {
        return collectionMedias;
    }

    public void setCollectionMedias(Vector<Media> collectionMedias) {
        this.collectionMedias = collectionMedias;
    }
}
