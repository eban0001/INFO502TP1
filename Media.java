public class Media {

    private String titre;
    private StringBuffer cote;
    private int note;

    private static String nomMediatheque;

    public Media() {
        this.titre = "";
        this.cote = new StringBuffer();
        this.note = 0;
    }

    public Media(String titre, StringBuffer cote, int note) {
        this.titre = titre;
        this.cote = cote;
        this.note = note;
    }

    public Media(Media autreMedia) {
        this.titre = autreMedia.titre;
        this.cote = new StringBuffer(autreMedia.cote.toString());
        this.note = autreMedia.note;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public StringBuffer getCote() {
        return cote;
    }

    public void setCote(StringBuffer cote) {
        this.cote = cote;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public static String getNomMediatheque() {
        return nomMediatheque;
    }

    public static void setNomMediatheque(String nomMediatheque) {
        Media.nomMediatheque = nomMediatheque;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Media media = (Media) obj;
        return note == media.note && titre.equals(media.titre) && cote.toString().equals(media.cote.toString());
    }

    @Override
    public String toString() {
        return "Media{titre='" + titre + "', cote='" + cote + "', note=" + note + "}";
    }
}
