public class TestFilm {

    public static void main(String[] args) {
        Film film1 = new Film();
        System.out.println("Film par défaut : " + film1);

        film1.setTitre("The Matrix");
        film1.setCote(new StringBuffer("SCI123"));
        film1.setNote(9);
        film1.setRealisateur("Lana Wachowski, Lilly Wachowski");
        film1.setAnnee(1999);

        System.out.println("\nFilm 1 après modification :");
        System.out.println(film1);

        Film film2 = new Film("Inception", new StringBuffer("MOV456"), 8, "Christopher Nolan", 2010);
        System.out.println("\nFilm 2 créé par initialisation :");
        System.out.println(film2);

        boolean sontEgaux = film1.equals(film2);
        System.out.println("\nLes films 1 et 2 sont-ils égaux ? " + (sontEgaux ? "Oui" : "Non"));

        Film film3 = new Film(film2);
        System.out.println("\nFilm 3 créé par copie de film2 :");
        System.out.println(film3);

        film3.setTitre("Interstellar");
        film3.setCote(new StringBuffer("MOV789"));
        film3.setNote(9);
        film3.setRealisateur("Christopher Nolan");
        film3.setAnnee(2014);

        System.out.println("\nFilm 3 après modification :");
        System.out.println(film3);

        System.out.println("\n--- Liste finale des films ---");
        System.out.println("Film 1 : " + film1);
        System.out.println("Film 2 : " + film2);
        System.out.println("Film 3 : " + film3);
    }
}
