public class Livre extends Media {

    private String auteur;
    private int nombrePages;

    public Livre() {
        super();
        this.auteur = "";
        this.nombrePages = 0;
    }

    public Livre(String titre, StringBuffer cote, int note, String auteur, int nombrePages) {
        super(titre, cote, note);
        this.auteur = auteur;
        this.nombrePages = nombrePages;
    }

    public Livre(Livre autreLivre) {
        super(autreLivre);
        this.auteur = autreLivre.auteur;
        this.nombrePages = autreLivre.nombrePages;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public int getNombrePages() {
        return nombrePages;
    }

    public void setNombrePages(int nombrePages) {
        this.nombrePages = nombrePages;
    }

    @Override
    public String toString() {
        return "Livre{titre='" + getTitre() + "', cote='" + getCote() + "', note=" + getNote() +
                ", auteur='" + auteur + "', nombrePages=" + nombrePages + "}";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        Livre livre = (Livre) obj;
        return nombrePages == livre.nombrePages && auteur.equals(livre.auteur);
    }
}
